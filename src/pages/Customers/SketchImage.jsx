import React from 'react';
import sketchfigure from "../../images/Group.png";
import styled from "styled-components";

function SketchFigure() {
    return (
        <SketchImage>
            <img className="sketchimage"
                 src={ sketchfigure }
                 alt="sketchimage"></img>
        </SketchImage>
    );
}

export default SketchFigure;

const SketchImage = styled.div`
.sketchimage{
  width: 300px;
  height: 400px;
  padding: 15px;
  margin-top: 50px;
}
`


