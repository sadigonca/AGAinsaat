
import React from 'react';
import styled from "styled-components";

function Buyer() {
    return (
        <div>
           <BuyerWrapper>
               <BuyerContainer>
                   <label htmlFor="Name-surname">Ad Soyad</label>
               </BuyerContainer>
               <BuyerContainer>
                   <label htmlFor="Fin-kod">FIN Kod</label>
               </BuyerContainer>
               <BuyerContainer>
                   <label htmlFor="payment">Ödəniş</label>
               </BuyerContainer>
               <BuyerContainer>
                   <label htmlFor="arayish">İş yerindən arayış
                   </label>
               </BuyerContainer>
           </BuyerWrapper> 
        </div>
    )
}

export default Buyer;

export const BuyerWrapper = styled.div``

export const BuyerContainer = styled.div``

export const label = styled.div``




