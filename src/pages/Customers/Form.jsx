import Buyer from "./Buyer";
import Payment from "./Payment";
import React from 'react';
import Voucher from "./Voucher";
import styled from "styled-components";

function Form() {
    return (
       <FormContainer>
           <FormHeader>
           <h1 className="buyer">Alıcı şəxs</h1>
           </FormHeader>
           <Buyer/>
           <Line />
           <Voucher />
           <Line /> 
           <Payment/>  
       </FormContainer> 
    )
}

export default Form;

const FormHeader = styled.div`
`

const FormContainer = styled.div`
`

const Line = styled.div``
