import React, { Suspense, lazy } from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

import { ClipLoader } from "react-spinners";

const GlobalStyle = lazy(() => import("../src/Utils/globalStyles"));
const Data = lazy(() => import("./components/Data/Data"));
const Navbar = lazy(() => import("./components/Navbar/Navbar"));
const Heading = lazy(() => import("./components/Header/Header"));
const SketchWrapper = lazy(() => import("./pages/Customers/SketchWrapper"));

function App() {
  return (
    <div className="wrapper">
      <Router>
        <Suspense
          fallback={
            <div className="loader-wrapper">
              <ClipLoader color="var(--main-color)" loading={true} size={150} />
            </div>
          }
        >
          <GlobalStyle />
          <div className="container">
            <Navbar />
            <div className="right-sidebar">
              <Heading />
              <Route exact path="/">
              </Route>
              <Route path="/customers">
                <SketchWrapper/>
              </Route>
              <Route path="/statistics">
              </Route>
            </div>
          </div>
        </Suspense>
      </Router>
    </div>
  );
}

export default App;