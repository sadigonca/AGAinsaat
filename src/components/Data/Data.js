import customers from "..//../images/customers.png";
import dots from "..//../images/dots.png";
import exit from "..//../images/exit.png";
import messages from "..//../images/messages.png";
import notifications from "..//../images/notification.png";
import settings from "..//../images/settings.png";
import statistics from "..//../images/statistics.png";

export const Cards = [
    {
      id: "1",
      area: "65m",
      sup: "2",
      color: "#FFFFFF",
    },
    {
        id: "2",
        area: "90m",
        sup: "2",
        color: "#E0CA55",
      },
      {
        id: "3",
        area: "120m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "4",
        area: "180m",
        sup: "2",
        color: "#313131",
      },
      {
        id: "5",
        area: "65m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "6",
        area: "900m",
        sup: "2",
        color: "#313131",
      },
      {
        id: "7",
        area: "120m",
        sup: "2",
        color: "#E0CA55",
      },
      {
        id: "8",
        area: "180m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "9",
        area: "65m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "10",
        area: "90m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "11",
        area: "120m",
        sup: "2",
        color: "#313131",
      },
      {
        id: "12",
        area: "180m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "13",
        area: "65m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "14",
        area: "180m",
        sup: "2",
        color: "#E0CA55",
      },
      {
        id: "15",
        area: "120m",
        sup: "2",
        color: "#E0CA55",
      },
      {
        id: "16",
        area: "180m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "17",
        area: "65m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "18",
        area: "90m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "19",
        area: "120m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "20",
        area: "180m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "21",
        area: "65m",
        sup: "2",
        color: "#E0CA55",
      },
      {
        id: "22",
        area: "90m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "23",
        area: "120m",
        sup: "2",
        color: "#313131",
      },
      {
        id: "24",
        area: "180m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "25",
        area: "65m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "26",
        area: "90m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "23",
        area: "120m",
        sup: "2",
        color: "#313131",
      },
      {
        id: "24",
        area: "180m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "25",
        area: "65m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "26",
        area: "90m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "27",
        area: "120m",
        sup: "2",
        color: "#313131",
      },
      {
        id: "28",
        area: "180m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "29",
        area: "65m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "30",
        area: "90m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "31",
        area: "120m",
        sup: "2",
        color: "#E0CA55",
      },
      {
        id: "32",
        area: "180m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "33",
        area: "65m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "34",
        area: "90m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "35",
        area: "120m",
        sup: "2",
        color: "#E0CA55",
      },
      {
        id: "36",
        area: "180m",
        sup: "2",
        color: "#313131",
      },
      {
        id: "37",
        area: "65m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "38",
        area: "90m",
        sup: "2",
        color: "#FFFFFF",
      },
      {
        id: "39",
        area: "120m",
        sup: "2",
        color: "#E0CA55",
      },
      {
        id: "40",
        area: "180m",
        sup: "2",
        color: "#313131",
      },
    ];
    
    
      export const CustomerData = [

        {
          customer: "Hacıyev Murad",
          building: "24",
          credit: "10 illik kredit",
          first_payment: "İlkin ödəniş",
          firspayment: "05.06.2019",
          month_payment: "Aylıq ödəniş",
          monthlypayment: "23 dekabr",
          statuscolor: "var(--yellow)",
          statustitle: "var(--other-color)",
          status: "Gecikir",
          button: "Bildiriş göndər",
          buttoncolor: "var(--main-color)",
          chevron: dots,
        },
          {
            customer: "Salmanov Nicat",
            building: "35",
            credit: "20 illik kredit",
            first_payment: "İlkin ödəniş",
            firspayment: "11.08.2018",
            month_payment: "Aylıq ödəniş",
            monthlypayment: "23 avqust",
            statuscolor: "var(--green)",
            statustitle: "var(--other-color)",
            status: "Ödənilib",
            chevron: dots,
          },
          {
            customer: "Hacıyev Muraad",
            building: "24",
            credit: "10 illik kredit",
            first_payment: "İlkin ödəniş",
            firspayment: "05.06.2019",
            month_payment: "Aylıq ödəniş",
            monthlypayment: "17 may",
            statuscolor: "var(--yellow)",
            statustitle: "var(--other-color)",
            status: "gecikir",
            button: "Bildiriş göndər",
            chevron: dots,
          },
          {
            customer: "Salmanov Nicat",
            building: "89",
            credit: "20 illik kredit",
            first_payment: "İlkin ödəniş",
            firspayment: "11.08.2018",
            month_payment: "Aylıq ödəniş",
            monthlypayment: "9 iyun",
            statuscolor: "var(--green)",
            statustitle: "var(--other-color)",
            status: "Ödənilib",
            chevron: dots,
          },
          {
            customer: "Hacıyev Muraad",
            building: "24",
            credit: "20 illik kredit",
            first_payment: "İlkin ödəniş",
            firspayment: "05.06.2019",
            month_payment: "Aylıq ödəniş",
            monthlypayment: "5 iyul",
            statuscolor: "var(--yellow)",
            statustitle: "var(--other-color)",
            status: "gecikir",
            button: "Bildiriş göndər",
            chevron: dots,
          },
          {
            customer: "Salmanov Nicat",
            building: "67",
            credit: "25 illik kredit",
            first_payment: "İlkin ödəniş",
            firspayment: "11.08.2018",
            month_payment: "Aylıq ödəniş",
            monthlypayment: "9 iyun",
            statuscolor: "var(--green)",
            statustitle: "var(--other-color)",
            status: "Ödənilib",
            chevron: dots,
          },
          {
            customer: "Salmanov Nicat",
            building: "12",
            credit: "30 illik kredit",
            first_payment: "İlkin ödəniş",
            firspayment: "11.08.2018",
            month_payment: "Aylıq ödəniş",
            monthlypayment: "16 oktyabr",
            statuscolor: "var(--green)",
            statustitle: "var(--other-color)",
            status: "Ödənilib",
            chevron: dots,
          },
          {
            customer: "Orucova Arzu",
            building: "67",
            credit: "30 illik kredit",
            first_payment: "İlkin ödəniş",
            firspayment: "11.08.2018",
            month_payment: "Aylıq ödəniş",
            monthlypayment: "16 may",
            statuscolor: "var(--green)",
            statustitle: "var(--other-color)",
            status: "Ödənilib",
            chevron: dots,
          },
      ];

      export const SideBarData = [
        {
          icon:statistics,
          title:"Statistika",
          path:"/",
        },
        {
          icon:customers,
          title:"Müştərilər",
          path:"/customers",
        },
        {
          icon:notifications,
          title:"Bildirişlər",
          path:"#!",
        },
        {
          icon:messages,
          title:"Mesajlar",
          path:"#!",
        },
        {
          icon:settings,
          title:"Parametrlər",
          path:"/settings",
        },
        {
          icon:exit,
          title:"Çıxış",
          path:"#!",
        },
      ]
