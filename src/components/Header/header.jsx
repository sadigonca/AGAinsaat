import React, { useState } from "react";

import Calendar from "../../images/calendar.png";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Search from "../../images/search.png";
import chevron from "../../images/chevron.png";
import { css } from "@emotion/react";
import notification from "../../images/black-notification.png";
import person from "../../images/profile.png";
import styled from "styled-components";

function Main() {
  const loading = useState(true);
  const [spinner, setSpinner] = useState(false);
  const [spinnerNot, setSpinnerNot] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSpinner = () => {
    setSpinner(!spinner);
    document
      .querySelector(".message-spinner")
      .classList.toggle("spinnerToggle");
  };
  const handleSpinnerNot = () => {
    setSpinnerNot(!spinnerNot);
    document
      .querySelector(".notification-spinner")
      .classList.toggle("spinnerToggle");
  };
  const override = css`
    display: block;
    margin: 0 auto;
  `;

  return (
    <Header>
      <NavContainer>
        <button className="add-new-project">
          Yeni Layihə əlavə et
          <i className="fas fa-plus"></i>
        </button>
        <div className="input-bar">
          <Input autoComplete="off" placeholder="Axtar"></Input>
          <img className="search" src={Search} alt="search-icn" />
        </div>
        <div className="person">
          <div className="calendar-group" onClick={handleSpinner}>
            <img className="calendar" src={Calendar} alt="calendar" />
          </div>
          <div className="spinner calendar-spinner">
            <div className="arrow_box"></div>
          </div>
          <img
            className="notification"
            src={notification}
            alt="notification"
            onClick={handleSpinnerNot}
          />
          <div className="spinner notification-spinner">
            <div className="arrow_box"></div>
          </div>
          <img src={person} alt="person" className="person-picture" />
          <button
            className="icon-button"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
          >
            <img src={chevron} alt="chevron" />
          </button>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            open={open}
            onClose={handleClose}
          >
            <MenuItem onClick={handleClose}>Profile</MenuItem>
            <MenuItem onClick={handleClose}>Logout</MenuItem>
          </Menu>
        </div>
      </NavContainer>
    </Header>
  );
}

export default Main;

export const Header = styled.div`
  background: #e5e5e5;
  position: sticky;
  top: 0;
  height: 7.5rem;
`;

const NavContainer = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: space-around;
  .search {
    position: absolute;
    right: 20px;
    background: var(--other-color);
  }

  .add-new-project {
    display: inline-block;
    cursor: pointer;
    padding: 14px 30px;
    font-size: 1rem;
    color: var(--other-color);
    background-color: var(--main-color);
    border: 1px solid var(--main-color);
    border-radius: 10px;

    .fas {
    float: right;
    margin-left: 10px;
    margin-top: 3px;
  }
  }

  .input-bar {
    display: flex;
    align-items: center;
    position: relative;
  }

  .person {
    display: flex;
    align-items: center;
    position: relative;

    .person-picture {
      margin: 0 10px;
    }

    .calendar-group {
      display: flex;
      align-items: center;
      justify-content: center;
      cursor: pointer;
    }
    .calendar {
      margin-right: 20px;
    }

    .notification {
      margin-right: 20px;
      cursor: pointer;
      position: relative;
    }
  }
  .icon-button {
    background: transparent;
    border: none;
    outline: none;
    cursor: pointer;
  }
`;

const Input = styled.input`
  position: relative;
  text-indent: 1rem;
  width: 33rem;
  height: 3rem;
  border: 1px solid var(--border-color);
  box-sizing: border-box;
  filter: drop-shadow(1px 2px 8px rgba(0, 0, 0, 0.25));
  border-radius: 0.4rem;
`;