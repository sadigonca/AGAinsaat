import React, { useState } from "react";

import ImageLogo from "../../images/logo-white.png";
import { Link } from "react-router-dom";
import { SideBarData } from "../Data/Data";
import styled from "styled-components";

function Navbar() {
  

  return (
    <Navigation>
      <div className="brand-name">
        <Logo src={ImageLogo} />
        <BrandName>AGA Inşaat</BrandName>
      </div>

      {SideBarData.map((el, key) => (
        <NavLinks key={key}>
          <NavLists >
            <img src={el.icon} alt="icons" />
            <Link to={el.path}>{el.title}</Link>
          </NavLists>
        </NavLinks>
      ))}
    </Navigation>
  );
}

export default Navbar;

const Navigation = styled.div`
  height: 100%;
  width: 20%;
  position: sticky;
  top: 0.5rem;
  background: var(--main-color);

  .brand-name {
    display: flex;
    align-items: center;
    margin: 40px 0 90px 70px;
  }
`;

const NavLinks = styled.div`
  padding: 0rem 4.25rem 0 4.25rem;
  border-top: 1px solid #3e3e3e;
`;

const NavLists = styled.div`
  display: flex;
  align-items: center;

  padding: 20px;
  transition: 0.3s all;
  

  img {
    margin-right: 1rem;
  }

  a {
    color: white;
    font-size: 1.2rem;
    text-align: center;
    text-decoration: none;

    &:hover {
      color: var(--yellow);
    }
  }
`;
const BrandName = styled.h1`
  margin-left: 1.25rem;
  font-family: inherit;
  font-size: 1.575rem;
  width: 50%;
  margin-top: 1.5625rem;
  line-height: 1;
  color: white;
`;

const Logo = styled.img`
  height: 4.8125rem;
`;